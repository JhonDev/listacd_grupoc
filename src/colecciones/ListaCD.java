package colecciones;

/**
 *
 * @author estudiante
 */
public class ListaCD<T> {
    
    private NodoD<T> cabeza;
    private int tamanio;

    public ListaCD() {
        
        this.cabeza=new NodoD();
        this.cabeza.setSig(cabeza);
        this.cabeza.setAnt(cabeza);
        //sobra:
        this.cabeza.setInfo(null);
        
        
    }

    
    public void insertarInicio(T info)
    {
    NodoD<T> nuevo=new NodoD<T>(info, this.cabeza.getSig(),this.cabeza);
    //redireccionar
    this.cabeza.getSig().setAnt(nuevo);
    this.cabeza.setSig(nuevo);
    
    this.tamanio++;
    }
    
    //O(1)
    public void insertarFinal(T info)
    {
    NodoD<T> nuevo=new NodoD<T>(info, this.cabeza,this.cabeza.getAnt());
    this.cabeza.setAnt(nuevo);
    nuevo.getAnt().setSig(nuevo);
    this.tamanio++;
    } 
    
    public void buscarNodo(){
        
    }
    
    
    @Override
    public String toString()
    {
    String msg="";
    for(NodoD<T> x=this.cabeza.getSig();x!=this.cabeza;x=x.getSig())
        msg+=x.getInfo()+"<->";
    return msg;
    }
   
    public boolean esVacio()
    {
        
    return (this.cabeza==this.cabeza.getSig());
    //return (this.cabeza==this.cabeza.getAnt());
    //return this.tamanio==0;
    }
    
    public int getTamanio() {
        return tamanio;
    }   
}